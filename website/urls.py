from django.conf.urls import url
from website import views

urlpatterns = [ 
    url(r'^rooms/$',views.Events.as_view(),name='events'),
    url(r'^about_us/$',views.Aboutus.as_view(),name='about_us'),
    url(r'^contact_us/$',views.Contact.as_view(),name='contact_us'),
    url(r'^rooms/$',views.Rooms.as_view(),name='rooms'),
]