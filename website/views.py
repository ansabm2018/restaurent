from django.shortcuts import render
from django.views import View

# Create your views here.

class Index(View):
    def get(self,request):
        return render(request,"web/website/index.html",{})

class Aboutus(View):
    def get(self,request):
        return render(request,"web/website/about.html")

class Events(View):
    def get(self,request):
        return render(request,"web/website/events.html",{})

class Contact(View): 
    def get(self,request):
        return render(request,"web/website/contact.html",{})  

class Rooms(View): 
    def get(self,request): 
        return render(request,"web/website/rooms.html",{})                

